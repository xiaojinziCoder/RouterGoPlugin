package com.xiaojinzi.routergo;

/**
 * 常量类
 */
public class Constants {

    public static String InterceptorAnnoClassName = "com.xiaojinzi.component.anno.InterceptorAnno";
    public static String RouterAnnoClassName = "com.xiaojinzi.component.anno.RouterAnno";
    public static String RouterClassName = "com.xiaojinzi.component.impl.Router";
    public static String RouterRequestBuilderClassName = "com.xiaojinzi.component.impl.RouterRequest.Builder";
    public static String RouterBuilderClassName = "com.xiaojinzi.component.impl.Navigator";
    public static String RxRouterBuilderClassName = "com.xiaojinzi.component.impl.RxRouter.Builder";
    public static String RxRouterClassName = "com.xiaojinzi.component.impl.RxRouter";
    public static String RouterHostMethodName = "host";
    public static String RouterHostAndPathMethodName = "hostAndPath";
    public static String RouterInterceptorNameMethodName = "interceptorNames";
    public static String RouterAnnoInterceptorName = "interceptorNames";
    public static String InterceptorAnnoValueName = "value";
    public static String RouterAnnoHostName = "host";
    public static String RouterAnnoPathName = "path";
    public static String RouterAnnoHostAndPathName = "hostAndPath";

    /*public static final String RouterName = "EHiRouter";
    public static final String RxRouterName = "EHiRxRouter";
    public static String RouterAnnoClassName = "com.ehi.component.anno.EHiRouterAnno";
    public static String RouterClassName = "com.ehi.component.impl.EHiRouter";
    public static String RouterRequestBuilderClassName = "com.ehi.component.impl.EHiRouterRequest.Builder";
    public static String RouterBuilderClassName = "com.ehi.component.impl.EHiRouter.Builder";
    public static String RxRouterBuilderClassName = "com.ehi.component.impl.EHiRxRouter.Builder";
    public static String RxRouterClassName = "com.ehi.component.impl.EHiRxRouter";
    public static String RouterHostMethodName = "host";
    public static String RouterAnnoHostName = "host";
    public static String RouterAnnoPathName = "value";*/

}
