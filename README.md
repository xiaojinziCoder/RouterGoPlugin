## 前言

这是一个 AndroidStudio/Idea 的辅助工具,是一个插件,让你使用组件化方案 
[Component](https://github.com/xiaojinzi123/Component) 的时候,可以像下面一样
![](./imgs/RouterGoPluginPreview.gif) 自由的跳转,不会因为使用组件化而导致需要
搜索字符串那样去查找

[点我下载插件](https://github.com/xiaojinzi123/RouterGoPlugin/releases)